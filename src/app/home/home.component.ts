import { PizzaService } from './../pizza.service';
import { Pizza } from './../models/pizza-model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  Pizzas: Pizza[];
  noPizzasFound: boolean;

  constructor(private pizzaService: PizzaService) { }

  getOutVal(id: number) {
    this.Pizzas = this.Pizzas.filter(pizza => pizza.id !== id);
  }

  getPizzas() {
    console.log(this.Pizzas);
    this.pizzaService.getPizzas().subscribe(
      (response: Pizza[]) => {
        if (response.length > 0) {
          this.Pizzas = response;
          this.noPizzasFound = false;
        } else {
          this.noPizzasFound = true;
        }
      }
    );
  }

  ngOnInit() {
    this.getPizzas();
  }
}
