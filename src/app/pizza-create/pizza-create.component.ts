import { Pizza } from './../models/pizza-model';
import { Component, OnInit } from '@angular/core';
import { PizzaService } from '../pizza.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { AllToppingsByType } from '../models/toppingdb-model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pizza-create',
  templateUrl: './pizza-create.component.html',
  styleUrls: ['./pizza-create.component.css']
})
export class PizzaCreateComponent implements OnInit {
  Pizza: Pizza;
  formGroup: FormGroup;
  nameMaxLength = 50;

  receivedAllTypes: string[];
  receivedAllSizes: string[];
  receivedToppings: AllToppingsByType[];
  receivedPriceCurency: string;

  dropdownSettings = {};
  toppingsData: string[];

  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder, private router: Router,
    // tslint:disable-next-line:align
    private activatedRoute: ActivatedRoute,
    // tslint:disable-next-line:align
    private pizzaService: PizzaService) {
    this.formGroup = this.formBuilder.group({
      id: '',
      type: ['', Validators.required],
      name: ['', [Validators.required, Validators.maxLength(this.nameMaxLength)]],
      imageUrl: ['', Validators.required],
      toppings: ['', Validators.required],
      requiredPrices: this.formBuilder.array([]),
      lastPrice: this.formBuilder.array([])
    });
  }

  get priceForm(): FormArray { return this.formGroup.get('requiredPrices') as FormArray; }
  get lastPriceForm(): FormArray { return this.formGroup.get('lastPrice') as FormArray; }

  // gets all pizza types, sizes and price currency
  getInformation() {
    forkJoin(
      this.pizzaService.getPizzaTypesForDropdown(), this.pizzaService.getAllPizzasSizes(),
      this.pizzaService.getPriceCurrency())
      .subscribe(
        ([types, sizes, curency]) => {
          this.receivedAllTypes = types;
          this.receivedAllSizes = sizes;
          this.receivedPriceCurency = curency;
          const pricePattern = /^\d+\.\d{0,2}$/;

          for (let size = 0; size < sizes.length - 1; size++) {
            const price = this.formBuilder.group({
              size: [sizes[size]],
              price: ['', [Validators.required, Validators.pattern(pricePattern)]]
            });
            // has all required sizes for the price form
            this.priceForm.push(price);
          }

          const lastPrice = this.formBuilder.group({
            largeSize: [sizes[sizes.length - 1]],
            largePrice: ['', Validators.pattern(pricePattern)]
          });
          // has all not required sizes for the price form
          this.lastPriceForm.push(lastPrice);
        });
  }

  // on type change gets toppings for selected type
  onTypeChange() {
    this.pizzaService.getToppingsForSelectedType(this.formGroup.get('type').value)
      .subscribe(
        (data: AllToppingsByType[]) => {
          if (data.length > 0) {
            this.receivedToppings = data;
            const toppingsForCheckBox = [];
            // gets toppings names for checkbox
            for (const topping of this.receivedToppings) {
              for (const toppingName of topping.contents) {
                toppingsForCheckBox.push({ id: toppingName, itemName: toppingName });
              }
            }
            this.toppingsData = toppingsForCheckBox;
          }
        });
  }

  postPizza() {
    const pizzaId = this.formGroup.get('id').value;
    const getPrices = [];
    const getSizes = [];
    const getToppings = [];
    const requiredPricesValues = this.formGroup.get('requiredPrices').value;
    const lastPriceValue = this.formGroup.get('lastPrice').value;
    const toppingsValues = this.formGroup.get('toppings').value;

    for (const price of requiredPricesValues) {
      getSizes.push(price.size);
      getPrices.push(Number(price.price));
    }

    if (lastPriceValue[0].largePrice !== '') {
      getSizes.push(lastPriceValue[0].largeSize);
      getPrices.push(Number(lastPriceValue[0].largePrice));
    }

    for (const topping of toppingsValues) {
      getToppings.push(topping.itemName);
    }

    // if there is a pizza id => delete the pizza and add the updated one
    if (pizzaId !== '') {
      this.httpClient.delete(`http://localhost:3000/pizzas/${pizzaId}`,
        {})
        .subscribe(result => console.log(result),
          err => console.error(err));
    }

    this.httpClient.post(`http://localhost:3000/pizzas/`,
      {
        type: this.formGroup.get('type').value,
        name: this.formGroup.get('name').value,
        imageUrl: this.formGroup.get('imageUrl').value,
        toppings: getToppings,
        prices: getPrices,
        priceCurrency: this.receivedPriceCurency,
        sizes: getSizes
      })
      .subscribe();
    this.returnHome();
  }

  // gets selected pizza
  getPizza(id: number) {
    this.pizzaService.getPizzaById(id)
      .subscribe(
        (pizza: Pizza) => this.editPizza(pizza),
        (err: any) => console.log(err)
      );
  }

  // gets information for edit form
  editPizza(pizza: Pizza) {
    this.formGroup.patchValue({
      type: pizza.type,
      name: pizza.name,
      imageUrl: pizza.imageUrl,
      sizes: pizza.sizes,
      priceCurrency: pizza.priceCurrency,
      id: pizza.id
    });

    const oldToppings = [];
    for (const toppingName of pizza.toppings) {
      oldToppings.push({ id: toppingName, itemName: toppingName });
    }
    this.formGroup.patchValue({
      toppings: oldToppings
    });

    const reqPrices = (this.formGroup.controls.requiredPrices as FormArray);
    let index = 0;
    for (const control of reqPrices.controls) {
      control.patchValue({
        price: pizza.prices[index].toFixed(2)
      });
      index++;
    }

    const lastPrice = (this.formGroup.controls.lastPrice as FormArray);
    for (const control of lastPrice.controls) {
      control.patchValue({
        largePrice: pizza.prices[pizza.prices.length - 1].toFixed(2)
      });
    }
  }

  returnHome() {
    this.router.navigate(['Home']);
  }

  ngOnInit() {
    this.getInformation();
    // setings for toppings dropdown
    this.dropdownSettings = {
      singleSelection: false,
      text: 'Select Toppings',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'myclass custom-class'
    };

    this.activatedRoute.paramMap.subscribe(params => {
      const pizzaId = +params.get('id');
      if (pizzaId) {
        this.getPizza(pizzaId);
      }
    });
  }
}

