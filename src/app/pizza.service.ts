import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Pizza } from './models/pizza-model';
import { AllPizzasByType } from './models/pizzadb-model';
import { AllToppingsByType } from './models/toppingdb-model';

@Injectable({
  providedIn: 'root'
})
export class PizzaService {

  constructor(private http: HttpClient) { }

  getPizzas(): Observable<Pizza[]> {
    return this.http.get<Pizza[]>(`http://localhost:3000/pizzas?_sort=name&_order=desc`);
  }

  getPizzaTypesForDropdown(): Observable<string[]> {
    return this.http.get<string[]>(`http://localhost:3000/types?_order=asc`);
  }

  getPizzasByType(type: string): Observable<AllPizzasByType[]> {
    return this.http.get<AllPizzasByType[]>(`http://localhost:3000/pizzas/?type=${type}`);
  }

  getAllPizzasSizes(): Observable<string[]> {
    return this.http.get<string[]>(`http://localhost:3000/sizes?_order=asc`);
  }

  getPriceCurrency(): Observable<string> {
    return this.http.get<string>(` http://localhost:3000/priceCurrency?_order=asc`);
  }

  getToppingsForSelectedType(type: string): Observable<AllToppingsByType[]> {
    if (type === 'vegan') {
      return this.http.get<AllToppingsByType[]>(`http://localhost:3000/toppings/?type=common`);
    } else if (type === 'vegetarian') {
      return this.http.get<AllToppingsByType[]>(`http://localhost:3000/toppings/?type=common&type=vegetarian`);
    } else {
      return this.http.get<AllToppingsByType[]>(`http://localhost:3000/toppings`);
    }
  }

  getSearchedPizzas(type: string, name?: string): Observable<Pizza[]> {
    return this.http.get<Pizza[]>(`http://localhost:3000/pizzas?type=${type}` +
      (name ? `&name=${name}` : ``));
  }

  getPizzaById(pizzaId): Observable<Pizza> {
    return this.http.get<Pizza>(`http://localhost:3000/pizzas/${pizzaId}`);
  }

  deletePizza(pizzaId): Observable<void> {
    return this.http.delete<void>(`http://localhost:3000/pizzas/${pizzaId}`);
  }
}
