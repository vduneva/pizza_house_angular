export class Pizza {
    id: number;
    name: string;
    toppings: string[];
    sizes: string[];
    prices: number[];
    priceCurrency: string;
    type: string;
    imageUrl: string;
}
