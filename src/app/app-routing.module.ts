import { PizzaCardComponent } from './pizza-card/pizza-card.component';
import { PizzaCreateComponent } from './pizza-create/pizza-create.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './home/home.component';
import {SearchComponent} from './search/search.component';

const routes: Routes = [
  {
    path: 'Home',
    component: HomeComponent
  },
  {
    path: 'Search',
    component: SearchComponent
  },
  {
    path: 'Create',
    component: PizzaCreateComponent
  },
  { path: '',
    redirectTo: '/Home',
    pathMatch: 'full'
  },
  {
    path: 'Edit/:id',
    component: PizzaCreateComponent
  },
  {
    path: 'Delete/:id',
    component: PizzaCardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
