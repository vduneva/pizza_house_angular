import { AllPizzasByType } from './../models/pizzadb-model';
import { PizzaService } from './../pizza.service';
import { Pizza } from './../models/pizza-model';
import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  selectedType: string;
  selectedName: string;
  selectedPrice: string;

  largeSize: string;
  // if there is large pizza's size => checkbox
  largeExist: boolean;
  isChecked: boolean;
  noPizzasFound: boolean;
  isFirstInit: boolean;

  receivedPizzasByType: AllPizzasByType[];
  receivedPizzas: Pizza[];
  receivedAllTypes: string[];

  pricesForSort = ['cheapest', 'most expensive'];
  isMoreThan1: boolean;

  constructor(private pizzaService: PizzaService) { }
  // get all pizza types for the dropdown in search
  getPizzaTypesForDropdown() {
    this.pizzaService.getPizzaTypesForDropdown()
      .subscribe(
        (data: string[]) => {
          this.receivedAllTypes = data;
        }
      );
  }

  // when pizza type changes dropdown in search shows all names with this type and checkbox if there is large pizza
  onTypeChange() {
    this.largeExist = false;
    this.selectedName = null;

    forkJoin(
      this.pizzaService.getPizzasByType(this.selectedType), this.pizzaService.getAllPizzasSizes()
    ).subscribe(
      ([response, allSizes]) => {
        this.receivedPizzasByType = response;
        this.largeSize = allSizes[allSizes.length - 1];

        // checks if there is pizza with largest size
        // tslint:disable-next-line:only-arrow-functions
        response.forEach(function (value) {
          if (value.sizes.find(c => c.valueOf() === this.largeSize)) {
            this.largeExist = true;
            return;
          }
        }.bind(this));
      });
  }

  // get searched pizzas by type, price, size and name
  getSearchedPizzas() {
    this.pizzaService.getSearchedPizzas(this.selectedType, this.selectedName)
      .subscribe(
        (response: Pizza[]) => {
          if (response.length > 0) {
            const largePizzas = [];

            if (this.isChecked) {
              // tslint:disable-next-line:only-arrow-functions
              response.forEach(function (value) {
                // adds all pizzas witth large size
                if (value.sizes[value.sizes.length - 1] === this.largeSize) {
                  largePizzas.push(value);
                }
              }.bind(this));

              if (largePizzas.length > 0) {
                this.receivedPizzas = largePizzas;
              } else {
                this.noPizzasFound = true;
                this.isFirstInit = false;
                return;
              }
            } else {
              this.receivedPizzas = response;
            }
            this.noPizzasFound = false;
            this.isMoreThan1 = this.receivedPizzas.length > 1;
          } else {
            this.noPizzasFound = true;
            this.isFirstInit = false;
          }
        });
  }

  sortPizzas() {
    if (this.selectedPrice === 'cheapest') {
      // tslint:disable-next-line:only-arrow-functions
      this.receivedPizzas.sort(function (a, b) { return a.prices[0] - b.prices[0]; });
    } else if (this.selectedPrice === 'most expensive') {
      // tslint:disable-next-line:only-arrow-functions
      this.receivedPizzas.sort(function (a, b) { return b.prices[0] - a.prices[0]; });
    }
  }

  getOutVal(id: number) {
    this.receivedPizzas = this.receivedPizzas.filter(pizza => pizza.id !== id);
  }

  ngOnInit() {
    this.getPizzaTypesForDropdown();
    this.noPizzasFound = true;
    this.isFirstInit = true;
  }
}
