import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { PizzaCardComponent } from './pizza-card/pizza-card.component';
import { PizzaService } from './pizza.service';
import { PizzaCreateComponent } from './pizza-create/pizza-create.component';
import { AngularMultiSelectModule } from '../../node_modules/angular2-multiselect-dropdown';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    PizzaCardComponent,
    PizzaCreateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    NgxPaginationModule
  ],
  providers: [PizzaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
