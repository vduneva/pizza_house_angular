import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pizza } from './../models/pizza-model';
import { PizzaService } from '../pizza.service';

@Component({
  selector: 'app-pizza-card',
  templateUrl: './pizza-card.component.html',
  styleUrls: ['./pizza-card.component.css']
})
export class PizzaCardComponent implements OnInit {
  @Input() Pizza: Pizza;

  @Output() pizzaIdToParent = new EventEmitter<number>();

  constructor(private pizzaService: PizzaService) {
  }

  delete(pizzaId: number) {
    if (confirm('Are you sure you want to delete the pizza')) {
      this.pizzaService.deletePizza(pizzaId)
        .subscribe(result => console.log(result),
          err => console.error(err));

      this.pizzaIdToParent.emit(pizzaId);
    }
  }

  ngOnInit() { }
}
